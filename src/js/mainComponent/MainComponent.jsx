import React from 'react';
import './mainComponent.less';
import Header from '../header/Header.jsx';
import Footer from '../footer/Footer.jsx';
import ContextConfig from '../context/context.jsx';
import quizQuestions from "../data/quizQuestions";
import Quiz from "../quiz/Quiz.jsx";
import Question from "../quiz/question/Question.jsx";
import Button from "../quiz/button/Button.jsx";

export default class MainComponent extends React.Component {
    static contextType = ContextConfig;

    state = {
        counter: 1,
        answersCount: quizQuestions,
        result: '',
    };

    incrementCount = () => {
        this.setState(state => ({counter: state.counter + 1}));
    };

    decrementCount = () => {
        this.setState(state => ({counter: state.counter - 1}));
    };

    finishQuiz = () => {
        const total = this.state.answersCount
            .map(question => question.answers.length)
            .reduce((sum, current) => sum + current, 0);

        const correct = this.state.answersCount
            .map(question => question.answers.filter(answer => answer.correct === answer.checked).length)
            .reduce((sum, current) => sum + current, 0);
        const result = (correct / total).toFixed(2);

        this.setState(() => ({
            result,
        }));
    };

    handleAnswerSelected = (event) => {
        const {
            name,
            value,
            checked: isChecked
        } = event.currentTarget;

        const isCorrect = JSON.parse(value);

        if (isCorrect) console.log('target');
        if (isCorrect) console.log(event.currentTarget);

        const index = this.state.counter - 1;
        const question = quizQuestions[index];
        const answerIndex = question.answers.findIndex(x => x.content === name);
        const answersCount = this.state.answersCount;

        answersCount[index].answers[answerIndex].checked = isChecked;

        this.setState(() => ({
            answersCount
        }));
    };

    render() {
        const {counter, result} = this.state;

        return (
            <React.StrictMode>
                <div className="page-wrapper">
                    <header className="header page-wrapper__header">
                        {this.context.modules.header.isActive && <Header counter={counter}/>}
                    </header>
                    <main className="content page-wrapper__content">
                        <Question content={quizQuestions[counter - 1].question}/>
                        <Quiz
                            answerOptions={quizQuestions[counter - 1].answers}
                            questionId={counter - 1}
                            question={quizQuestions[counter - 1].question}
                            questionTotal={quizQuestions.length}
                            onAnswerSelected={this.handleAnswerSelected}
                        />
                        <div className='content page-wrapper__buttons'>
                            <Button
                                className='custom-button'
                                callback={this.decrementCount}
                                name={'Prev'}
                                disabled={counter === 1}/>
                            <Button
                                className='custom-button'
                                callback={this.incrementCount}
                                name={'Next'}
                                disabled={counter === quizQuestions.length}/>
                            <Button
                                className='custom-button'
                                callback={this.finishQuiz}
                                name={'Finish'}
                                disabled={false}/>
                        </div>
                        <div className='content page-wrapper__result'>
                            {result ? `Your result is ${result}!` : ''}
                        </div>
                    </main>
                    <footer className="footer page-wrapper__footer">
                        {this.context.modules.header.isActive && <Footer/>}
                    </footer>
                </div>
            </React.StrictMode>

        );
    }
}
