import React from 'react';
import logo from 'assets/react-logo.svg';
import './header.less';
import quizQuestions from "../data/quizQuestions";

import PropTypes from 'prop-types';
import Counter from "../quiz/Counter.jsx";


export default class Header extends React.Component {
    render() {
        const { counter } = this.props;

        return (
            <React.Fragment>
                <div className="logo-container header__logo-container">
                    <img className='logo-container__logo' src={logo} alt='logo'/>
                </div>
                <div className="title-container header__title-container">
                    <span className="title-container__text">{'REACT QUIZ'}</span>
                </div>
                <div className="title-container__text">
                    <Counter counter={counter} total={quizQuestions.length}/>
                </div>
            </React.Fragment>
        );
    }
}

Header.propTypes = {
    counter: PropTypes.number.isRequired,
};