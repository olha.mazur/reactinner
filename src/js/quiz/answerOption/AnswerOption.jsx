import React from 'react';
import PropTypes from 'prop-types';
import './answerOption.less';


const  AnswerOption = props => {
    const { name, correct, checked, onAnswerSelected } = props;

    return (
        <li className="answerOption">
            <input
                type='checkbox'
                name={name}
                value={correct}
                checked={checked}
                onChange={onAnswerSelected}
            />
            <label className="checkBoxLabel" htmlFor={name}>
                {props.answerContent}
            </label>
        </li>
    );
};

AnswerOption.propTypes = {
    correct: PropTypes.bool.isRequired,
    checked: PropTypes.bool.isRequired,
    answerContent: PropTypes.string.isRequired,
    onAnswerSelected: PropTypes.func.isRequired
};

export default AnswerOption;