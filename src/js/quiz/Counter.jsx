import React from 'react';
import PropTypes from 'prop-types';


const Counter = props => {
    const { counter, total} = props;
    return(
        <div className='counter'>Question {counter} from {total}</div>
    );
};

Counter.propTypes = {
    counter: PropTypes.number.isRequired,
    total: PropTypes.number.isRequired,
};

export default Counter;