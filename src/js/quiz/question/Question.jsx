import React from  'react';
import PropTypes from 'prop-types';
import './question.less';


const Question = props => {
    const { content } = props;
    return(
        <div className='question'>{content}</div>
    );
};

Question.propTypes = {
    content: PropTypes.string.isRequired
};

export default Question;