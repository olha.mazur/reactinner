import React from 'react';
import PropTypes from 'prop-types';
import AnswerOption from "./answerOption/AnswerOption.jsx";
import './quiz.less';


class Quiz extends React.Component {
    renderAnswerOptions = (key) => {
        return (
            <AnswerOption
                key={key.content}
                number={key.index}
                name={key.content}
                answerContent={key.content}
                correct={key.correct}
                checked={key.checked}
                onAnswerSelected={this.props.onAnswerSelected}
            />
        );
    };

    render() {
        return (
            <div className='answers-wrapper'>
                {this.props.answerOptions.map(this.renderAnswerOptions)}
            </div>
        );
    }
}

export default Quiz;


Quiz.propTypes = {
    answerOptions: PropTypes.array.isRequired,
    question: PropTypes.string.isRequired,
    questionId: PropTypes.number.isRequired,
    onAnswerSelected: PropTypes.func.isRequired
};
