import React from 'react';
import PropTypes from 'prop-types';
import './button.less';

const Button = props => {

    const {
        name,
        callback,
        className,
        disabled,
    } = props;

    return (
        <button className={className}
                onClick={callback && callback}
                disabled={disabled}>
            {name}
        </button>
    );
};

Button.propTypes = {
    className: PropTypes.string,
    callback: PropTypes.func.isRequired,
    name: PropTypes.string,
    disabled: PropTypes.bool.isRequired,
};

export default Button;