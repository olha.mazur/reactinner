const quizQuestions = [
    {
        question: "Virtual DOM:",
        answers: [
            {
                checked: false,
                correct: true,
                content: "Updates faster than Real DOM."
            },
            {
                checked: false,
                correct: true,
                content: "Can’t directly update HTML."
            },
            {
                checked: false,
               correct: false,
                content: "Creates a new DOM if element updates."
            },
            {
                checked: false,
                correct: true,
                content: "Manipulates DOM very easy."
            },
        ]
    },
    {
        question: "What is React?",
        answers: [
            {
                checked: false,
                correct: true,
                content: "React is a front-end JavaScript library developed by Facebook in 2011."
            },
            {
                checked: false,
                correct: true,
                content: "It follows the component based approach which helps in building reusable UI components."
            },
            {
                checked: false,
                correct: true,
                content: "It is used for developing complex and interactive web and mobile UI."
            },
        ]
    },
    {
        question: "Is React a full-blown framework?",
        answers: [
            {
               checked: false,
               correct: false,
               content: "Yes."
            },
            {
                checked: false,
                correct: true,
                content: "No, React is just a library."
            },
        ]
    },
    {
        question: "Can browsers read JSX?",
        answers: [
            {
                checked: false,
                correct: true,
                content: "No. First we need to transform JSX file into a JavaScript object" +
                    " using JSX transformers like Babel and then pass it to the browser."
            },
            {
                checked: false,
                correct: false,
                content: "Yes."
            },
        ]
    },
    {
        question: "Props in React:",
        answers: [
            {
                checked: false,
                correct: true,
                content: "Is the shorthand for Properties in React."
            },
            {
                checked: false,
                correct: true,
                content: "Are read-only components which must be kept pure i.e. immutable."
            },
            {
                checked: false,
                correct: false,
                content: "They are always passed down from the child to the parent component throughout the application."
            },
            {
                checked: false,
                correct: false,
                content: "A parent component can never send a prop back to the child component."
            },
        ]
    },
    {
        question: "State in React:",
        answers: [
            {
                checked: false,
                correct: false,
                content: "Are the source of data and must be kept as complex as possible."
            },
            {
                checked: false,
                correct: true,
                content: "Are the objects which determine components rendering and behavior."
            },
            {
                checked: false,
                correct: false,
                content: "Are immutable like the props."
            },
            {
                checked: false,
                correct: true,
                content: "Are mutable unlike the props and create dynamic and interactive components."
            },
        ]
    },
    {
        question: "Arrow functions in React:",
        answers: [
            {
                checked: false,
                correct: true,
                content: "Are more of brief syntax for writing the function expression."
            },
            {
                checked: false,
                correct: true,
                content: "Allow to bind the context of the components properly since in ES6 auto binding is not available by default."
            },
        ]
    },
    {
        question: "Stateless Component:",
        answers: [
            {
                checked: false,
                correct: true,
                content: "Do not have the authority to change state"
            },
            {
                checked: false,
                correct: false,
                content: "Contains knowledge of past, current and possible future state changes"
            },
            {
                checked: false,
                correct: false,
                content: "Stores info about component’s state change in memory"
            },
        ]
    },
    {
        question: "Stateful Component",
        answers: [
            {
                checked: false,
                correct: false,
                content: "Do not have the authority to change state"
            },
            {
                checked: false,
                correct: true,
                content: "Contains the knowledge of past, current and possible future changes in state"
            },
            {
                checked: false,
                correct: true,
                content: "Stateless components notify them about the requirement of the state change, then they send down the props to them."
            },
        ]
    },
    {
        question: "What are the different phases of React component’s lifecycle?",
        answers: [
            {
                checked: false,
                correct: true,
                content: "Initial Rendering Phase."
            },
            {

                checked: false,
                correct: true,
                content: "Updating Phase."
            },
            {
                checked: false,
                correct: true,
                content: "Unmounting Phase."
            },
        ]
    },
];

export default quizQuestions;